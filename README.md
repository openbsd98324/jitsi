# Meet Jit si

https://meet.jit.si/

the ping of the server is very good.

For ARM, i.e. raspberry pi, one possible issue/problem with Jitsi is that it needs a very up to date webbrowser. 
If firefox is kinda old, it will not work on a raspberry pi.

**It needs a very good PC, otherwise it will lag and be slow., and it will not work.**

![](medias/jitsi.png)


## 1.) Working on following OS/Distro/Release

### 1.a.) Working on OpenSUSE Leap on AMD Ryzen 3 !

Slow but OK, working.

Firefox cannot enable to select which mic. 
Mic does not work. 

So installation of Chromium
with Version 95.0.4638.54 (openSUSE Build) stable (64-bit)
Get help with Chromium

wget -c --no-check-certificate   "https://gitlab.com/openbsd98324/opensuse-leap-15.3/-/raw/main/rootfs/opensuse-leap-15.3-amd64-x86_64-preinstalled-KDE-notebook.tar.gz"

````
Linux localhost.localdomain 5.3.18-57-default #1 SMP Wed Apr 28 10:54:41 UTC 2021 (ba3c2e9) x86_64 x86_64 x86_64 GNU/Linux
````


Packages (see pub directory)

**mic + speaker + webcam = working **

### 1.b.) Half Working on ARM Raspberry PI model 4 

It will work with Chromium with :
with default web browser (installed).  
You can see with chromium but selection of Mic will not work.

https://downloads.raspberrypi.org/raspios_armhf/images/raspios_armhf-2021-11-08/2021-10-30-raspios-bullseye-armhf.zip
(note apt-get broken, fix into /var needed)

 
** only  speaker + webcam = working **

````
Linux PITV 5.4.72-v7l+ #1356 SMP Thu Oct 22 13:57:51 BST 2020 armv7l GNU/Linux

````


````
pi@PITV:~ $ dpkg -l | grep chromium
ii  chromium                              90.0.4430.212-1~deb10u1                armhf        web browser
rc  chromium-browser                      92.0.4515.98~buster-rpt2               armhf        Chromium web browser, open-source version of Chrome
ii  chromium-codecs-ffmpeg-extra          92.0.4515.98~buster-rpt2               armhf        Extra ffmpeg codecs for the Chromium Browser
ii  chromium-common                       90.0.4430.212-1~deb10u1                armhf        web browser - common resources used by the chromium packages
pi@PITV:~ $ dpkg -l | grep firefox
ii  firefox-esr                           78.15.0esr-1~deb10u1+rpi1              armhf        Mozilla Firefox web browser - Extended Support Release (ESR)
````



##  2.) Not working

#### Not Working on OpenSUSE Leap on EEEPC (PC too slow, with n3050) and KDE.


### Not-Working on Linux on Distro/Release: 

It will not work with: 

- Devuan ASCII 

-   firefox-esr                            68.10.0esr-1~deb9u1                amd64        Mozilla Firefox web browser - Extended Support Release (ESR)




 



